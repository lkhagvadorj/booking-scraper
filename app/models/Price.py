from app.extensions import Base
from sqlalchemy.dialects.mysql import BIGINT, TIMESTAMP, TINYINT, BOOLEAN, DATE
from sqlalchemy.types import DECIMAL
from sqlalchemy.sql import func, text
from sqlalchemy import Column, String, ForeignKey


class Price(Base):

    __tablename__ = 'prices'

    id = Column(BIGINT(unsigned=True), primary_key=True)

    hotel_id = Column(
            BIGINT(unsigned=True), ForeignKey("hotels.id"),
            index=True, nullable=False
        )

    room_name = Column(String(80), nullable=False)
    sleeps = Column(TINYINT(unsigned=True), nullable=False)
    wallet = Column(String(4), nullable=False)
    is_breakfast_included = Column(BOOLEAN(), nullable=False)
    price = Column(DECIMAL(precision=18, scale=2, asdecimal=True), nullable=True)
    crawled_date = Column(DATE(), server_default=text('CURRENT_DATE()'))

    created_at = Column(TIMESTAMP(timezone=True, fsp=6), index=True, server_default=text('CURRENT_TIMESTAMP(6)'))
