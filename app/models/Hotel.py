from app.extensions import Base
from sqlalchemy.dialects.mysql import BIGINT, TIMESTAMP
from sqlalchemy.types import DECIMAL
from sqlalchemy.sql import func, text
from sqlalchemy import Column, String


class Book(Base):

    __tablename__ = 'hotels'

    id = Column(BIGINT(unsigned=True), primary_key=True)
    name = Column(String(80), nullable=False)
    
    address = Column(String(110), nullable=False)
    location = Column(String(191), nullable=False)
    rating = Column(DECIMAL(precision=4, scale=2, asdecimal=True), nullable=True)

    created_at = Column(TIMESTAMP(timezone=True, fsp=6), server_default=text('CURRENT_TIMESTAMP(6)'))
    updated_at = Column(TIMESTAMP(timezone=True, fsp=6), server_default=text('CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)'))
