from app.config import APP_CONFIG
from redis import Redis
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base

redis_client = Redis(host=APP_CONFIG['REDIS_HOST'], port=APP_CONFIG['REDIS_PORT'], password=APP_CONFIG['REDIS_PASS'])
# db_engine = create_engine(APP_CONFIG['SQLALCHEMY_DATABASE_URI'], **APP_CONFIG['SQLALCHEMY_ENGINE_OPTIONS'])
Base = declarative_base()
