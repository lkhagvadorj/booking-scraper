from app.config import APP_CONFIG
from app.helpers.functions import current_date_format, now
from app.extensions import redis_client
from app.exceptions import ParserException
from bs4 import BeautifulSoup
from functools import wraps
from json import dumps, loads
import logging
import re
from requests import Session


class Booking:

    def __init__(self) -> None:

        self.url = 'https://www.booking.com/'
        self.headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'sec-ch-ua': '".Not/A)Brand";v="99", "Google Chrome";v="101", "Chromium";v="101"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'Sec-Fetch-Dest': 'document',
            'Sec-Fetch-Mode': 'navigate',
            'Sec-Fetch-Site': 'none',
            'Sec-Fetch-User': '?1',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/516.58 (KHTML, like Gecko) Chrome/101.0.0.0 Safari/516.58'
        }
        self.http = Session()
        self.soup = None
        self.per_page = 25


    def wrapper(func):

        @wraps(func)
        def wrap(self, *args, **kwargs):
            try:
                func(self, *args, **kwargs)
            except ParserException as e:
                logging.error(str(e))
            except Exception as e:
                logging.error(str(e))

        return wrap

    def __parse_hotel_count(self, location):

        logging.debug(f'{location}: action=hotel_count, finding arp-header...')

        p = self.soup.find('div', attrs={'data-component': 'arp-header'})
    
        if not p:
            raise ParserException(
                f'{location}: action=hotel_count, arp-header not found.'
            )

        if not p.find('h1'):
            raise ParserException(
                f'{location}: action=hotel_count, properties tag not found.'
            )

        text = p.find('h1').string
        hotel_counts = re.findall(r'\w+: (\d+) properties', text)

        if len(hotel_counts) != 1:
            raise ParserException(
                f'{location}: action=hotel_count, hotel count not found, ' + text
            )
        
        return hotel_counts[0]

    def __parse_pagination(self, location):
        
        logging.debug(f'{location}: action=pagination, finding pagination...')
        logging.debug(f'{location}: action=pagination, finding arp-properties-list...')

        p = self.soup.find('div', attrs={'data-component': 'arp-properties-list'})

        if not p:
            raise ParserException(
                f'{location}: action=pagination, arp-properties-list not found.'
            )

        if p.find('nav') and p.find('nav').find('ol'):
            page_counts = len(p.find('nav').find('ol').find_all('li'))
        else:
            raise ParserException(
                f'{location}: action=pagination, pagination not found.'
            )

        return page_counts

    def __parse_hotel(self, location):

        hotels = []

        logging.debug(f'{location}: action=hotel, finding hotels...')
        logging.debug(f'{location}: action=hotel, finding arp-properties-list...')

        p = self.soup.find('div', attrs={'data-component': 'arp-properties-list'})

        if not p:
            raise ParserException(
                f'{location}: action=hotel, arp-properties-list not found.'
            )

        # ! I think data-testid="property-card" attribute is not reliable.
        # ! We need more alternate mechanism.

        divs = p.find_all('div', attrs={'data-testid': 'property-card'})
        
        if len(divs) == 0:
            raise ParserException(
                f'{location}: action=hotel, property-cards not found.'
            )

        for i, div in enumerate(divs):

            # logging.debug(f'{location}: parsing hotel {i + 1}')

            try:
                div = div.find_all('div', recursive=False)[0].find_all('div', recursive=False)[1]
            except Exception as e:
                raise ParserException(
                    f'{location}: action=hotel, data part not found.'
                )

            if not div.find('a'):
                raise ParserException(
                    f'{location}: action=hotel {i + 1}, link not found.'
                )

            if not div.find('a').div:
                raise ParserException(
                    f'{location}: action=hotel {i + 1}, name not found.'
                )

            if not div.find('a').div.string:
                raise ParserException(
                    f'{location}: action=hotel {i + 1}, name not found.'
                )

            link = div.find('a')['href']
            name = div.find('a').div.string

            hotels.append({
                'link': link,
                'name': name,
            })

            logging.info(f'{location}: [+] {name} found.')

        return hotels

    def __switch_to_next_page(self, location, params, offset):
        
        params = dict(params)
        params['offset'] = offset

        try:
            response = self.http.get(
                'https://www.booking.com/searchresults.html',
                headers=self.headers, params=params, timeout=15
            )
        except Exception as e:
            raise Exception(
                f'{location}: action=http_access, HTTP Request failed => {str(e)}'
            )

        if response.status_code != 200:
            raise Exception(
                f'{location}: action=http_access, Received HTTP Status Code: {response.status_code}'
            )

        if 'text/html' not in response.headers['Content-Type']:
            raise Exception(
                f'{location}: action=http_access, Expected HTML Page.'
            )

        self.soup = BeautifulSoup(response.text, 'html.parser')

    def __find_hotels(self, location):

        hotels = []
        logging.debug(f'{location}: action=http_access, beginning...')

        # ! label, sid, dest_id утгууд хэрхэн үүсэж байгааг олж мэдэх.

        params = {
            'label': 'gen173nr-1DCAEoggI46AdIM1gEaJYBiAEBmAExuAEXyAEM2AED6AEB-AECiAIBqAIDuAKdpZSWBsACAdICJDYzMDZlMWIzLWMxMmEtNDNiMy04NjRiLTVkNzAzYWQ2NDMzN9gCBOACAQ',
            'sid': 'e854848dd0b7b9b071f7f8cad66b89ff',
            'sb': '1',
            'sb_lp': '1',
            'src': 'index',
            'src_elem': 'sb',
            'error_url': 'https://www.booking.com/index.html?label=gen173nr-1DCAEoggI46AdIM1gEaJYBiAEBmAExuAEXyAEM2AED6AEB-AECiAIBqAIDuAKdpZSWBsACAdICJDYzMDZlMWIzLWMxMmEtNDNiMy04NjRiLTVkNzAzYWQ2NDMzN9gCBOACAQ&sid=e854848dd0b7b9b071f7f8cad66b89ff&sb_price_type=total&&',
            'ss': location,
            'is_ski_area': '0',
            'ssne': location,
            'ssne_untouched': location,
            'dest_id': '-2353539',
            'dest_type': 'city',
            'checkin_year': now().year,
            'checkin_month': now().month,
            'checkin_monthday': now().day,
            'checkout_year': now().year,
            'checkout_month': now().month,
            'checkout_monthday': now().day + 1,
            'group_adults': '2',
            'group_children': '0',
            'no_rooms': '1',
            'b_h4u_keep_filters': '',
            'from_sf': '1',
        }

        try:
            response = self.http.get(
                'https://www.booking.com/searchresults.html',
                headers=self.headers, params=params, timeout=15
            )
        except Exception as e:
            raise Exception(
                f'{location}: action=http_access, HTTP Request failed => {str(e)}'
            )

        if response.status_code != 200:
            raise Exception(
                f'{location}: action=http_access, Received HTTP Status Code: {response.status_code}'
            )

        if 'text/html' not in response.headers['Content-Type']:
            raise Exception(
                f'{location}: action=http_access, Expected HTML Page.'
            )

        self.soup = BeautifulSoup(response.text, 'html.parser')
        
        # ? Find hotel count.
        hotel_counts = self.__parse_hotel_count(location)
        logging.info(f'{location}: action=hotel_count, {hotel_counts} hotels found.')

        # ? Find pages
        page_counts = self.__parse_pagination(location)
        logging.info(f'{location}: {page_counts} pages found.')

        # ? Find hotels
        for page in range(1, page_counts + 1):
            logging.debug(f'{location}: Parsing page {page}...')
            if page != 1:
                self.__switch_to_next_page(location, params, self.per_page * (page - 1))
            hotels += self.__parse_hotel(location)

        return hotels

    @wrapper
    def crawl_hotel(self, locations):
        
        for location in locations:

            logging.info(f'===============================================')
            logging.info(f'Crawling hotels in {location}.')
            logging.info(f'{current_date_format()} >:')

            hotels = self.__find_hotels(location)

            try:
                redis_client.rpush(APP_CONFIG['QUEUE_NAME'], dumps(hotels))
                logging.info(f'{location} pushed to queue.')
            except Exception as e:
                logging.error(f'{location} queue failed => {str(e)}')

    def crawl_rooms(self, hotels):

        for hotel in loads(hotels):
            logging.info(hotel['name'])

        # ! Crawl room data
        # ! Save to database
