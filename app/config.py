from dotenv import load_dotenv
from os import environ


load_dotenv()

APP_CONFIG = {

    'APP_ENV': environ.get('APP_ENV', 'dev'),
    'APP_DEBUG':  environ.get('APP_DEBUG', 'False') == 'True',

    'CRAWLER_TIMEOUT': 60 * 1,  # 1 Minutes.

    'REDIS_HOST': environ['REDIS_HOST'],
    'REDIS_PORT': int(environ['REDIS_PORT']),
    'REDIS_PASS': environ['REDIS_PASS'],

    'SQLALCHEMY_ENGINE_OPTIONS': {
        'encoding': 'utf8',
        'max_overflow': 5000,
        'pool_size': 0,
        'pool_recycle': 600,
        'pool_timeout': 30,
        'pool_pre_ping': True,
        'query_cache_size': 1000,
    },

    'SQLALCHEMY_DATABASE_URI': 'mysql://%s:%s@%s:%d/%s' % (
        environ['DB_USER'],
        environ['DB_PASS'],
        environ['DB_HOST'],
        int(environ['DB_PORT']),
        environ['DB_NAME'],
    ),

    'QUEUE_NAME': environ['QUEUE_NAME']
}