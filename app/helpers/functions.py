
from datetime import datetime


def current_date_format():
    return datetime.now().strftime('%Y-%m-%d')


def now():
    return datetime.now()
