import logging
import os, time
from sys import argv
from app.config import APP_CONFIG
from app.crawler import Booking
from app.extensions import redis_client
# os.environ['TZ'] = 'Asia/Ulaanbaatar'
# time.tzset()

logging.basicConfig(
    level=logging.DEBUG if APP_CONFIG['APP_DEBUG'] else logging.info,
    format='%(asctime)s\t%(levelname)s\t%(message)s'
)


def run_hotel_crawler():

    logging.info('Hotel mode running...')
    locations = ['Ulaanbaatar']

    while True:
        Booking().crawl_hotel(locations)
        time.sleep(APP_CONFIG['CRAWLER_TIMEOUT'])


def run_room_crawler():

    logging.info('Room mode running...')

    while True:
        try:
            hotels = redis_client.lpop(APP_CONFIG['QUEUE_NAME'])
        except Exception as e:
            logging.error('hotel fetch failed.')
            time.sleep(1)
            continue
        
        if not hotels:
            time.sleep(1)
            continue

        print(hotels)

        Booking().crawl_rooms(hotels)

def run():

    if argv[1] == 'hotel':
        run_hotel_crawler()
    elif argv[1] == 'room':
        run_room_crawler()
    else:
        logging.warning(f"{APP_CONFIG['WORKER_MODE']} not detected...")

if __name__ == '__main__':
    run()
